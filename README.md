# OVERVIEW

This is a work-in-progress extension to `ggplot2`.  The intent is to provide a simpler, standardized approach to producing combination heatmap / contour plots.

# EXAMPLE CODE + OUTPUTS

The basic case is creating a single panel

```{r}
require(ggplot2)
refdf <- within(
  expand.grid(x=seq(-1,1,by=0.1), y=seq(-0.5,0.5,by=0.01)),
    zebra <- y+x^3
  )
ggplot(refdf) + aes(x, y) + geom_contoured_heatmap(zebra, refdf) + scale_contoured_heatmap()
```

![single panel plot](single.png)

and the extension should also support facet plots:

```{r}
refdf2 <- within(
  expand.grid(x=seq(-1,1,by=0.1), y=seq(-0.5,0.5,by=0.01), offsetcol = 1:3, offsetrow = 1:3),
  zebra <- y + x^3 + offsetcol - offsetrow
)
ggplot(refdf2) + aes(x, y) + facet_grid(offsetrow ~ offsetcol) +
  geom_contoured_heatmap(zebra, refdf2) + scale_contoured_heatmap()
```

![facet plot](facet.png)

# TODOs

0. Define some edge cases, and ensure the `(geom|scale)_...` isn't breaking on them.

0. Also, internal refactoring: so that `(geom|scale)_...` don't need duplicate arguments, smarter approach to generating breaks, smarter use of computed elements from `geom_contour`, etc.

In general, make the interface more `ggplot`ish:

1. Particularly, `geom_contoured_heatmap()` should not need to receive the main plot data, and should work off just having a `z` specified in the main `aes(...)` call.

2. Provide more arguments for the relevant settings, like color / fill scales and so on.

Stretch:

3. Integrate the legends. I'd like the contour annotations (both color + linetype) to appear directly on the colorbar, rather than as separate legends.  The general colorbar tick marks could appear on one side, and contour ticks on the other.

4. Support other kinds of contour differentiation, e.g. alpha channel instead linetype.

5. Contour labels.

# KNOWN BUGS

1. The linetypes are currently not mirroring around neutral point correctly.
